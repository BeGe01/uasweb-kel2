<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create(['username' => 'tester', 'name' => 'Tester', 'foto_profil' => 'default.jpg', 'password' => Hash::make('tester123')]);
        User::create(['username' => 'tester2', 'name' => 'Tester 2', 'foto_profil' => 'default.jpg', 'password' => Hash::make('tester123')]);
        User::create(['username' => 'tester3', 'name' => 'Tester 3', 'foto_profil' => 'default.jpg', 'password' => Hash::make('tester123')]);
    }
}
