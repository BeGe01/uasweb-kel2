<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Likes extends Model
{
    use HasFactory;
    protected $fillable = [
        'id_pengguna',
        'id_postingan',

    ];

    public function username()
    {
        return $this->belongsTo(User::class,'id_pengguna');
    }
    public function like()
    {
        return $this->belongsTo(User::class,'id_postingan');
    }

    public static function allData()
    {
        // query builder
        return DB::table("likes")->get();
    }
}
