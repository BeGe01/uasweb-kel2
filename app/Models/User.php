<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'username',
        'name',
        'bio',
        'foto_profil',
        'password',
    ];

    public function following()
    {
        return $this->hasMany(Follow::class,'id_pengguna', 'id')->pluck('id_following'); // Return ID Following
    }
    
    public function followers()
    {
        return $this->hasMany(Follow::class,'id_following', 'id')->pluck('id_pengguna'); // Return ID Followers
    }

    public function jumlahFollowing()
    {
        return $this->hasMany(Follow::class,'id_pengguna', 'id')->count(); // Return jumlah following
    }

    public function jumlahFollowers()
    {
        return $this->hasMany(Follow::class,'id_following', 'id')->count(); // Return jumlah followers
    }

    public function likes()
    {
        return $this->hasMany(Likes::class,'id_pengguna', 'id')->pluck('id_postingan'); // Return ID Postingan yang di like
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];
}
